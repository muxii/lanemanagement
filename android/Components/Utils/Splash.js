import React from 'react'
import {
    StyleSheet,
    View,
    Image,
    Text
  } from 'react-native';
  import { SimpleAnimation } from 'react-native-simple-animations';

export const Splash = () => {
    return (
        
        <View style={styles.SplashScreen_RootView}>  
       
        <View style={styles.SplashScreen_ChildView}>  
            <SimpleAnimation delay={1000} duration={2000} fade >
            <Image source={require('../../../assets/logo.png')} />  
            </SimpleAnimation>
            </View>  
                
        </View>
       
    )
}
const styles = StyleSheet.create(  
    {  
        // MainContainer:  
        // {  
        //     flex: 1,  
        //     justifyContent: 'center',   
        //     paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0  
        // },  
       
        SplashScreen_RootView:  
        {  
            justifyContent: 'center',  
            flex:1,  
            // margin: 10,  
            position: 'absolute',  
            width: '100%',  
            height: '100%',  
          },  
       
        SplashScreen_ChildView:  
        {  
            justifyContent: 'center',  
            alignItems: 'center',  
            backgroundColor: '#00BBDC',  
            flex:1,  
        },  
    });  



export default Splash;
