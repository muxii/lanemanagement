import React,{useEffect, useState,Component} from 'react'
import {
    View,
    Text,
    TextInput,
    StyleSheet,
    Button,
    Alert,
    KeyboardAvoidingView
  } from 'react-native';
import { Redirect } from 'react-router-native';
import Splash from '../Utils/Splash';


 const Login = () => {
  
    const [visible,setVisible] =useState(true);
    const [value, setValue] = useState('');
    const [password, setPassword] = useState('');
    const [redirecting,setRedirecting]=useState(false)

    useEffect(() => {
        setTimeout(()=>{  
            setVisible(false)  
          }, 5000);
    }, [])
   
  
   if(redirecting){
       return(<Redirect to="main" />)
   }

    return (
      <>
        { visible ?  
        <Splash/>
        :
        <KeyboardAvoidingView
        behavior={Platform.OS == "ios" ? "padding" : "height"}
        style={styles.container}
      > 
        <View style={styles.RootView}> 
            <Text style={styles.text}>
                USER NAME
            </Text>       
            <TextInput
                style={{ height: 40,borderRadius:5,shadowColor:'grey', borderColor: 'gray',marginBottom:20, borderWidth: 1 }}
                onChangeText={text => setValue(text)}
                value={value}   
            />
             <Text style={styles.text}>
                PASSWORD
            </Text>
            <TextInput
                textContentType="password"
                style={{ height: 40,borderRadius:5,shadowColor:'grey', borderColor: 'gray', borderWidth: 1 }}
                onChangeText={text => setPassword(text)}
                value={password}   
            />
            <View style={styles.btn}>
             <Button
                buttonStyle={{
                    fontSize:18
                }}
                textStyle={{
                    fontSize:18
                }}
                title="LOGIN"
                onPress={()=>setRedirecting(true)}
             />
             </View>
        </View>
        </KeyboardAvoidingView>
    }
    </>
    )
}

export const styles = StyleSheet.create(  
    {  
        // MainContainer:  
        // {  
        //     flex: 1,  
        //     justifyContent: 'center',   
        //     paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0  
        // },  
        container:{
            flex:1
        },
        RootView:  
        {  
            justifyContent: 'center',  
            flex:1,  
            padding:50,
            // margin: 10,  
            // position: 'absolute',  
            width: '100%',  
            height: '100%',  
          }, 
          btn:{
            marginTop:20,
            
          } ,
        text:{
            marginTop:10,
            color: "#00BBDC", 
            fontSize: 20,
            shadowOpacity: 0.5,
            shadowRadius: 3,
            shadowOffset: {
            height: 0,
            width: 0,
            },
            elevation: 2,
        },
        SplashScreen_ChildView:  
        {  
            justifyContent: 'center',  
            alignItems: 'center',  
            backgroundColor: '#00BBDC',  
            flex:1,  
        },  
    });
export default  Login;
