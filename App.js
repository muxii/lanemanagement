/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import { NativeRouter, Route } from 'react-router-native';
import Login from './android/Components/Login/Login';
import { Main } from './android/Components/Main/Main';
import { Loader } from './android/Components/Utils/Loader';

const App: () => React$Node = () => {
  return (
    <>
    
  <NativeRouter>
      <Route exact path="/" component={Login} />
      <Route path="/main" component={Main} />
      
  </NativeRouter>
    </>
  );
};


export default App;
